import { qs } from "../externals";

export function generateNext(pathname: string, search?: string) {
  if (search === undefined) {
    search = "";
  }
  return `${pathname}${search}`;
}

export interface INextReturn {
  pathname: string;
  search?: string;
}

export function getNext(search?: string): INextReturn {
  if (search) {
    const query = qs.parse(search);
    if (query.next) {
      let next: string = query.next as string;
      next = next.replace(/"/g, "");
      let pathname: string = "/";
      let search: string | undefined = undefined;
      if (next.indexOf("?") <= 0) {
        pathname = next;
      } else {
        [pathname, search] = next.split("?", 2);
      }
      return {
        pathname,
        search,
      };
    }
  }
  return {
    pathname: "/",
  };
}
